<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RecipeController extends Controller
{
    public function show($id) {
        return view('recipes.view',['recipe' => \App\Recipe::find($id)]);
    }
    public function showAll() {
        return view("recipes.recipes");
    }
    public function showJSON() {
        return json_encode(\App\Recipe::all());
    }
}
