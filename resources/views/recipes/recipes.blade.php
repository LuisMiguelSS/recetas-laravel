<p>Recipes</p>

<ul>

    @foreach (\App\Recipe::all() as $recipe)
        <li>
            <a href="{{ url('recipes/' . $recipe->id) }}">{{$recipe->title}}</a>
        </li>
    @endforeach

</ul>
    {{json_encode(\App\Recipe::all())}}
